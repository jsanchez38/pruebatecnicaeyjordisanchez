## Prueba Tecnica EY Jordi
En proyecto de prueba de maquetación se ha realizado siguiendo BEM, con una estructura
y organización diferentes a la de Alejandro, utilizando el framework en la parte mobile de [Tailwindcss](https://tailwindcss.com/).
Aún así, hemos trabajado conjuntamente en algunos puntos haciendo cada uno su propia nav
bar.
Hemos querido hacer dos proyectos diferentes para que así veais dos maneras de trabajar
como también nuestra capacidad de adaptarnos e improvisar según lo demandado.
## Estructura del proyecto:
src<br />
 - Assets -> Imágenes del proyecto<br />
 - Components :
        - DesktopComponent -> Componente creado con BEM <br />
        - MobileComponent -> Componente creado con base tawling <br />
 - Styles | Sass | Abstracto 
                - Mixins -> Contiene los querys responsive<br />
                             - _variables -> Contiene variables globales como primer color, segundo color, breakpoints responsive.<br />
                 -Components -> Carpeta donde irán todos los scss<br />
                                - DesktopComponent.scss -> Estilos del componente desktop<br />
 - App.scss -> Scss general de la aplicación<br />
 - App.vue -> Contiene métodos para renderizar el componente Mobile o Desktop dependiendo del rize que tenga la pantalla<br />
 
 
## Project setup
```
nvm use
nvm install [Si la versión de node no es la correcta]
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
